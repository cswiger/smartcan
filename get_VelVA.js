#!/usr/bin/env node
//
// socketcan works with node v8.17.0 lts/carbon
// $ nvm use lts/carbon
// Now using node v8.17.0 (npm v6.13.4)
// $ node -v
// v8.17.0
//
// In one second - volts/amps are reported 10 times, or every 100mSec
// while velocity is reported 49 times, 5 times as much or every 20mSec
// 
// this script keeps track of the last second data was reported and only updates on a new second

var can = require('socketcan')
var channel = can.createRawChannel("can0", true);

// keep track of previous time of report
var d = new Date();
var dA = Math.floor(d.getTime()/1000);
var dV = dA;
var dE = dA;

function parsemsg(msg) {
  var d = new Date(0);
  d.setUTCSeconds(msg.ts_sec)
  if (msg.id == 0x508 & dA != Math.floor(d.getTime()/1000) ) { 
    console.log("Amps",d,( ( (msg.data[2]&0x3f)*0x100 + msg.data[3]) -0x2000) /10.);
    dA = Math.floor(d.getTime()/1000);
  }
  if (msg.id == 0x448 & dV != Math.floor(d.getTime()/1000) ) {
    console.log("Volts",d,(msg.data[6]*0x100 + msg.data[7])/10.);
    dV = Math.floor(d.getTime()/1000);
  }
  if (msg.id == 0x200 & dE != Math.floor(d.getTime()/1000) ) {
    console.log("Velocity",d,(msg.data[2]*0x100 + msg.data[3]));
    dE = Math.floor(d.getTime()/1000);
  }
}

channel.addListener("onMessage", parsemsg);

channel.start()

