#!/usr/bin/env node
//
// socketcan works with node v8.17.0 lts/carbon
// $ nvm use lts/carbon
// Now using node v8.17.0 (npm v6.13.4)
// $ node -v
// v8.17.0
//

var can = require('socketcan')

var channel = can.createRawChannel("can0", true);

function parsemsg(msg) {
  // have to instantiate this every usage 
  var d = new Date(0);
  d.setUTCSeconds(msg.ts_sec)
  if (msg.id == 0x508) { 
    console.log("Amps",d,( ( (msg.data[2]&0x3f)*0x100 + msg.data[3]) -0x2000) /10.);
  }
  if (msg.id == 0x448) {
    console.log("Volts",d,(msg.data[6]*0x100 + msg.data[7])/10.);
  }
}

channel.addListener("onMessage", parsemsg);
channel.start()



