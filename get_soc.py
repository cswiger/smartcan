#!/usr/bin/env python3

import can, sys

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)

while True:
   try:
     resp = bus.recv()
     if(resp.arbitration_id == 0x518):
        print("State of charge: ",resp.data[7]/2.)
   except KeyboardInterrupt:
     sys.exit(0)


