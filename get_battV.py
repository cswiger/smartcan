#!/usr/bin/env python3
# 
# gets and prints cell voltage for all 93 of them
# 
# using iso-tp  https://web.archive.org/web/20180701051607/http://canbushack.com/iso-15765-2
#  python actually too slow, but responds to the First Frame with 30 00 20  - continue at 20mSec
# then just saves a big bunch of can traffic as it comes to sort thru offline
# 

import can

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)
battV_request_message = can.Message(data=[3, 0x22, 0x02, 0x08, 0, 0, 0, 0],arbitration_id=0x7E7,extended_id=0)

# store 2048 can bus messages
msg = []
bus.send(battV_request_message)
for ndx in range(2048):
   resp = bus.recv()
   msg.append(resp)
   if(resp.data[0:5].hex() == '1193620208'): 
      nxtmsg = can.Message(extended_id=0, arbitration_id=0x07e7, data=[0x30, 0, 20, 0, 0, 0, 0, 0]) 
      bus.send(nxtmsg)

# save only those with id 0x7ef
result = []
for resp in msg:
  if (resp.arbitration_id == 0x7ef):
    result.append(resp.data.hex())

# cut to the battery data and strip off the PCI (protocol control info) bytes and command success echo 
store = result[0][5*2:]
for idx in range(1,len(result)):
  store += result[idx][2:]

# then convert every 4 bytes to float voltage
for idx in range(0,4*93,4):
  print(int(store[idx:idx+4],16)/1000.)



