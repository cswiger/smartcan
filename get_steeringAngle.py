#!/usr/bin/env python3

import can, sys

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)

while True:
   try:
     resp = bus.recv()
     if(resp.arbitration_id == 0x0C2):
        val = resp.data[1]*256+resp.data[0]
        if (val > 32767): val = 32768 - val
        print("Steering angle: ",val)
        #print(resp.data[0],resp.data[1])
   except KeyboardInterrupt:
     sys.exit(0)


