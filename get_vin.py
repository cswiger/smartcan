#!/usr/bin/env python3

import can

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)
vin_request_message = can.Message(data=[3, 0x22, 0xF1, 0x90, 0, 0, 0, 0],arbitration_id=0x7E7,extended_id=0)

vin = ""

msg = []
bus.send(vin_request_message)
# collect 256 can messages to get all the reply frames with the VIN
for ndx in range(256):
   resp = bus.recv()
   msg.append(resp)
   if(resp.data[0:5].hex() == '101462f190'):	# this is the iso-tp first frame response 1x with length 014 and command + 40	
      nxtmsg = can.Message(extended_id=0, arbitration_id=0x07e7, data=[0x30, 0, 50, 0, 0, 0, 0, 0])	# 62 F1 90 - send flow control 30 to send all with 50 mSec timing
      bus.send(nxtmsg)

for resp in msg:
   if (resp.arbitration_id == 0x7EF):
     if(resp.data[0:5].hex() == '101462f190'):
        vin += resp.data[-3:].decode()
     if(resp.data.hex()[0:2] == '21'):
        vin += resp.data[1:].decode()
     if(resp.data.hex()[0:2] == '22'):
        vin += resp.data[1:].decode()

print(vin)

