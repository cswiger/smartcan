#!/usr/bin/env node
//
// socketcan works with node v8.17.0 lts/carbon
// $ nvm use lts/carbon
// Now using node v8.17.0 (npm v6.13.4)
// $ node -v
// v8.17.0
//



var can = require('socketcan')
var channel = can.createRawChannel("can0", true);

// var to build vehicle identification number up in 
var vin = '';

// need a small delay after channel.start() and sending requests
function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

// request for VIN message
msgout = { id : 0x7e7, ext: false, data : Buffer([ 3, 0x22, 0xF1, 0x90, 0, 0, 0, 0 ]) }

// transport protocol flow control to get rest of VIN data, at 50mSec intervals per frame
fcout = { id : 0x7e7, ext: false, data : Buffer([0x30, 0, 10, 0, 0, 0, 0, 0]) }

function parsereply(msg) {
  if (msg.id == 0x7ef) {
    // console.log(msg.data);
    if (msg.data.toString('hex').substring(0,2) == '21' | msg.data.toString('hex').substring(0,2) == '22' ) {
      vin += msg.data.toString().substring(1,8);
    }
    if (msg.data.toString('hex').substring(0,10) == '101462f190') {
      channel.send(fcout);
      vin += msg.data.toString().substring(4,7);
    }
  }
}


async function main() {
	channel.addListener("onMessage", parsereply);
	channel.start();
	await sleep(50);
	channel.send(msgout);
	await sleep(50);
	console.log(vin);
        process.exit();
}

main()


