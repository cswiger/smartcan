#!/usr/bin/env python3

import can, sys

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)

while True:
   try:
     resp = bus.recv()
     if(resp.arbitration_id == 0x318):
        print("Range: %0.2f" % (resp.data[7]*.621371))
   except KeyboardInterrupt:
     sys.exit(0)


