#!/usr/bin/env node
//
// socketcan works with node v8.17.0 lts/carbon
// $ nvm use lts/carbon
// Now using node v8.17.0 (npm v6.13.4)
// $ node -v
// v8.17.0
//
// 0x3d5 low voltage battery is sent 50 times / second, every 20mSec

var can = require('socketcan')

var channel = can.createRawChannel("can0", true);

function parsemsg(msg) {
  // have to instantiate this every usage 
  var d = new Date(0);
  d.setUTCSeconds(msg.ts_sec)
  if (msg.id == 0x3d5) { 
    console.log("LV battery",d,msg.data[3]/10.);
  }
}

channel.addListener("onMessage", parsemsg);
channel.start()



