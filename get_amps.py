#!/usr/bin/env python3

import can, sys

baud = 500000

bus = can.interface.Bus(channel='can0', bustype='socketcan_native', bitrate=baud)

while True:
   try:
     resp = bus.recv()
     if(resp.arbitration_id == 0x508):
        print("Amps: ",(((resp.data[2]&0x3f)*0x100 + resp.data[3]) - 0x2000)/10.)
   except KeyboardInterrupt:
     sys.exit(0)


